from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy import create_engine
import os


BASEDIR = 'static/upload'

Base = declarative_base()

engine = create_engine(os.getenv('SQLALCHEMY_DATABASE_URI'))#, echo = True, connect_args = {"check_same_thread": False})

Session = sessionmaker(bind = engine)

session = Session()

# создает экземпляр create_engine в конце файла  
 


