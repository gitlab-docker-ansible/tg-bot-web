from init_app import init_flask
import os

if __name__ == "__main__":
    init_flask.run(host=os.getenv('FLASK_RUN_HOST'))