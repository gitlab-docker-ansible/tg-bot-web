from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, FileField, SelectField, FloatField
from wtforms.validators import DataRequired, Email
from admin.models import Category 


class ProductForm(FlaskForm):
	name = StringField("Название:", validators=[DataRequired()])
	category = SelectField("Выберите категорию:",
                               choices=[])
	description = TextAreaField("Описание:")
	price = FloatField("Стоимость:")
	image = FileField("Загрузите изображение:")

class ProductFormEdit(FlaskForm):
	name = StringField("Название:")
	category = SelectField("Выберите категорию:",
                               choices=[])
	description = TextAreaField("Описание:")
	price = FloatField("Стоимость:")
	image = FileField("Загрузите изображение:")