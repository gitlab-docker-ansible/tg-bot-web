from flask import Flask
import os
import time
from sqlalchemy.exc import OperationalError
from database import Base, engine, BASEDIR
from log import logger
from admin.views import admin
from main.bot_telegram import start_bot



def init_flask():
	app = Flask(__name__)
	#app.config['FLASK_RUN_HOST'] = os.getenv('FLASK_RUN_HOST')
	app.config['SECRET_KEY'] = 'oiernviuer9erjv09e8jeu9rvniervnje'
	app.config['UPLOAD_FOLDER'] = BASEDIR
	app.config['FLASK_APP'] = os.getenv('FLASK_APP')
	logger.info('Program started')
	cn = True
	logger.info('Test connect with database')
	for i in range(1, 11):
		try:
			Base.metadata.create_all(engine)
		except OperationalError as er:
			cn = False
			logger.warning('Try = {}, Error = {}'.format(i, er))
			time.sleep(10)
		if cn == True:
			break
		if i == 10:
			logger.warning('Database error')
	#start_bot()
	app.register_blueprint(admin)
	return app
