from email import message
import os
import re
from main.my_inline_query import inline_query_1
from database import session
from admin.models import Category
from log import logger
from telegram import (
    ReplyKeyboardMarkup, 
    ReplyKeyboardRemove, 
    Update, 
    InlineKeyboardButton, 
    InlineKeyboardMarkup
    )

from telegram.constants import ParseMode
from admin.models import Category, Product


#from config import TOKEN
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    InlineQueryHandler,
    ConversationHandler,
    MessageHandler,
    filters,
    CallbackQueryHandler
)


# GENDER, PHOTO, LOCATION, BIO = range(4)
(
    NAME, 
    TELEPHONE, 
    DATE, 
    GUESTS, 
    FORMAT, 
    DURATION, 
    LOCATION, 
    KITCHEN, 
    BUDGET,
    POZHELANIYA,
    COMPLITE
) = range(11)
(
    START_ROUTES,
    STOP_ROUTES
) = range(2)
(
    INCREASE,
    DECREASE,
    CONFIRM,
    END_PRODUCT
) = range(4)

main_keyboard = [
    ["Просмотреть меню"],
    ["Сделать заказ"],
    ["Связаться с нами", "Оставить отзыв"],
]
main_markup = ReplyKeyboardMarkup(main_keyboard, one_time_keyboard=True, resize_keyboard=True)


keyboard_for_order = [
    ["Age", "Favourite colour"],
    ["Number of siblings", "Something else..."],
    ["Done"],
]
order_markup = ReplyKeyboardMarkup(keyboard_for_order, one_time_keyboard=True)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """Start Для того что бы максимально точно ответить на вопрос <<Сколько стоит банкет или фуршет?>>"""
    await update.message.reply_text(
        "Здравствуйте!",
        reply_markup=main_markup,
    )

async def get_category(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """  """
    category = session.query(Category).all()
    keybord = [[InlineKeyboardButton(cat.name, switch_inline_query_current_chat=cat.name)] for cat in session.query(Category).all()]

    reply_markup = InlineKeyboardMarkup(keybord)

    await update.message.reply_text("Выберите категорию:", reply_markup=reply_markup)


async def prod_descript(update: Update, context: ContextTypes.DEFAULT_TYPE):
    text = update.message.text
    await context.bot.delete_message(update.message.chat_id,
                                       update.message.message_id)
    text = text.replace('h7yf83f8gfrv098owpow0e08u', '')
    prod = session.query(Product).filter(Product.name == text).first()
    keybord = [
        [
            InlineKeyboardButton("-", callback_data=str(DECREASE)), 
            InlineKeyboardButton(str(1), callback_data=str(1)), 
            InlineKeyboardButton("+", callback_data=str(INCREASE))
        ],
        [
            InlineKeyboardButton('Добавить в корзину', callback_data=str(CONFIRM))
        ]
    ]
    context.user_data['count'] = 1
    context.user_data['product'] = prod
    reply_markup = InlineKeyboardMarkup(keybord)
    caption = f"""<b>{prod.name}</b>\n{prod.description}\n{prod.price} тенге"""
    await context.bot.sendPhoto(chat_id=update.message.chat_id, 
                                photo=os.getenv('PHOTO_URL')+prod.image, 
                                caption=caption, parse_mode=ParseMode.HTML,
                                reply_markup=reply_markup)
    return START_ROUTES

async def increase(update: Update, context: ContextTypes.DEFAULT_TYPE):
    prod = context.user_data['product']
    context.user_data['count'] = context.user_data['count'] + 1
    count = context.user_data['count']
    keybord = [
        [
            InlineKeyboardButton("-", callback_data=str(DECREASE)), 
            InlineKeyboardButton(str(count), callback_data=str(count)), 
            InlineKeyboardButton("+", callback_data=str(INCREASE))
        ],
        [
            InlineKeyboardButton('Добавить в корзину', callback_data=str(CONFIRM))
        ]
    ]
    query = update.callback_query
    await query.answer()
    reply_markup = InlineKeyboardMarkup(keybord)
    caption = f"""<b>{prod.name}</b>\n{prod.description}\n{prod.price*count} ₸"""
    await context.bot.sendPhoto(chat_id=query.message.chat.id, 
                                photo=os.getenv('PHOTO_URL')+prod.image, 
                                caption=caption, parse_mode=ParseMode.HTML,
                                reply_markup=reply_markup)
    return START_ROUTES


async def decrease(update: Update, context: ContextTypes.DEFAULT_TYPE):
    prod = context.user_data['product']
    if context.user_data['count'] > 1:
        context.user_data['count'] = context.user_data['count'] - 1
    count = context.user_data['count']
    keybord = [
        [
            InlineKeyboardButton("-", callback_data=str(DECREASE)), 
            InlineKeyboardButton(str(count), callback_data=str(count)), 
            InlineKeyboardButton("+", callback_data=str(INCREASE))
        ],
        [
            InlineKeyboardButton('Добавить в корзину', callback_data=str(CONFIRM))
        ]
    ]
    query = update.callback_query
    await query.answer()
    reply_markup = InlineKeyboardMarkup(keybord)
    caption = f"""<b>{prod.name}</b>\n{prod.description}\n{prod.price*count} ₸"""
    await context.bot.sendPhoto(chat_id=query.message.chat.id, 
                                photo=os.getenv('PHOTO_URL')+prod.image, 
                                caption=caption, parse_mode=ParseMode.HTML,
                                reply_markup=reply_markup)
    return START_ROUTES


async def confirm_prod(update: Update, context: ContextTypes.DEFAULT_TYPE):
    prod = context.user_data['product']
    context.user_data['count'] = context.user_data['count']
    count = context.user_data['count']
    keybord = [
        [
            InlineKeyboardButton('Вернутся назад', callback_data=str(END_PRODUCT))
        ]
    ]
    query = update.callback_query
    await query.answer()
    reply_markup = InlineKeyboardMarkup(keybord)
    caption = f"""<b>{prod.name}</b>\n\n{prod.description}\n\n{prod.price*count} ₸\n\n Вы успешно дабавили товар в корзину"""
    await context.bot.sendPhoto(chat_id=query.message.chat.id, 
                                photo=os.getenv('PHOTO_URL')+prod.image, 
                                caption=caption, parse_mode=ParseMode.HTML,
                                reply_markup=reply_markup)
    return STOP_ROUTES


async def end_product(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Handle the inline query. This is run when you type: @botusername <query>"""
    query = update.callback_query
    await query.answer()

    await query.message.reply_text("Главное меню",
        reply_markup=main_markup,
    )

    return ConversationHandler.END



async def order(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Start the conversation and asks the user their name."""
    
    await update.message.reply_text(
        "Как Вас зовут?",
        reply_markup=ReplyKeyboardMarkup(
            [[update.message.from_user.first_name]], 
            resize_keyboard=True,
            one_time_keyboard=True
        ),
    )
    return NAME




async def name(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the name and asks tel."""
    text = update.message.text
    context.user_data["name"] = text
    await update.message.reply_text(
        "Введите, пожалуйста, номер телефона для связи",
        reply_markup=ReplyKeyboardRemove(),
    )

    return TELEPHONE


async def telephone(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the telephone and asks for a date."""
    text = update.message.text
    context.user_data["telephone"] = text
    await update.message.reply_text(
        "Введите дату проведения мероприятия"
        "\n*например, 25 сентябрь 2022",
        reply_markup=ReplyKeyboardRemove(),
    )

    return DATE


async def ivent_date(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the date and asks for a guests."""
    text = update.message.text
    context.user_data["date"] = text
    await update.message.reply_text(
        "Предпологаемое количество гостей",
        reply_markup=ReplyKeyboardRemove(),
    )

    return GUESTS


async def guests(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Store the guests and ask for a ivent format"""
    text = update.message.text
    context.user_data["guests"] = text
    await update.message.reply_text(
        "Формат мероприятия (нужное подчеркнуть,"
        " а лучше подробно описать: банкет, фуршет,"
        " выездное обслуживание, только доставка,"
        " свадьба, юбилей, корпоратив, День рождения и т.д.)"
    )
    return FORMAT


async def ivent_format(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the guests and asks for a format."""
    text = update.message.text
    context.user_data["format"] = text
    await update.message.reply_text(
        "Продолжительность праздника в часах. Чтобы"
        " понимать, какое количество блюд мы можем предложить."
        "\n *нужно ввести число",
        reply_markup=ReplyKeyboardRemove(),
    )

    return DURATION


async def duration(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the format and asks for a duration."""
    text = update.message.text
    context.user_data["duration"] = text
    await update.message.reply_text(
        "Локация мероприятия или адрес доставки блюд.",
        reply_markup=ReplyKeyboardRemove(),
    )

    return LOCATION


async def location(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the duration and asks for a location."""
    text = update.message.text
    context.user_data["location"] = text
    await update.message.reply_text(
        "Наличие кухни на площадке, если нет — мы привезём с собой.",
        reply_markup=ReplyKeyboardMarkup(
            [["Есть", "Нет"]], 
            resize_keyboard=True, 
            one_time_keyboard=True
        ),
    )

    return KITCHEN


async def kitchen(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the location and asks for a kitchen."""
    text = update.message.text
    context.user_data["kitchen"] = text
    await update.message.reply_text(
        "Примерный бюджет на персону."
        "\n*в сумах",
        reply_markup=ReplyKeyboardRemove(),
    )

    return BUDGET


async def budget(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the kitchen and asks for a budget."""
    text = update.message.text
    context.user_data["budget"] = text
    await update.message.reply_text(
        "Ваши пожелания: Ваши пожелания: возможно, кто-то из гостей вегетарианец, или не ест определенные продукты, пишите все, что нужно знать о вас и ваших гостях."
    )

    return POZHELANIYA


async def pozhelaniya(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the budget and asks for a pozhelaniya."""
    ans = update.message.text
    context.user_data["pozhelaniya"] = ans
    text = context.user_data
    dt = """
    <strong>Ваша заявка сохранена, мы свяжемся с вами в ближайшее время.</strong>
    -----------------------------------------------------
    <b>Детали заказа</b>
    Имя: {}
    Номер: {}
    Дата проведения: {}
    Количество предпологаемых гослей: {}
    Формат мероприятия: {}
    Продолжительность: {} часов
    Место проведения: {}
    Наличие кухни: {}
    Примерный бюджет на персону: {} сумов
    Пожелания: {}
    """.format(
        text["name"], 
        text["telephone"],
        text["date"],
        text["guests"],
        text["format"],
        text["duration"],
        text["location"],
        text["kitchen"],
        text["budget"],
        text["pozhelaniya"]
        )


    await update.message.reply_text(dt, parse_mode='html', reply_markup=main_markup)

    return ConversationHandler.END


async def complit_hand(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stores the pozhelaniay and complite handler"""
    text = context.user_data
    await update.message.reply_text(
        reply_markup=main_markup,
    )

    return ConversationHandler.END


def start_bot() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    logger.info('Start telegram BOT')
    application = Application.builder().token(os.getenv("TELEGRAM_TOKEN")).build()

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[MessageHandler(filters.Regex("^Сделать заказ$"), order)],
        states={
            NAME: [MessageHandler(filters.TEXT, name)],
            TELEPHONE: [MessageHandler(filters.TEXT, telephone)],
            DATE: [MessageHandler(filters.TEXT, ivent_date)],
            GUESTS: [MessageHandler(filters.TEXT, guests)],
            FORMAT: [MessageHandler(filters.TEXT, ivent_format)],
            DURATION: [MessageHandler(filters.TEXT, duration)],
            LOCATION: [MessageHandler(filters.TEXT, location)],
            KITCHEN: [MessageHandler(filters.TEXT, kitchen)],
            BUDGET: [MessageHandler(filters.TEXT, budget)],
            POZHELANIYA: [MessageHandler(filters.TEXT, pozhelaniya)],
            COMPLITE: [MessageHandler(filters.TEXT, complit_hand)]
        },
        fallbacks=[CommandHandler("cancel", complit_hand)],
    )

    conv_handler_prod = ConversationHandler(
        entry_points = [MessageHandler(filters.Regex("h7yf83f8gfrv098owpow0e08u$"), prod_descript)],
        states = {
            START_ROUTES: [
                CallbackQueryHandler(increase, pattern="^" + str(INCREASE) + "$"),
                CallbackQueryHandler(decrease, pattern="^" + str(DECREASE) + "$"),
                CallbackQueryHandler(confirm_prod, pattern="^" + str(CONFIRM) + "$")
            ],
            STOP_ROUTES: [
                CallbackQueryHandler(end_product, pattern="^" + str(END_PRODUCT) + "$")
            ]
        },
        fallbacks=[MessageHandler(filters.Regex("^oerihf98uj9ori$"), end_product)]
    )

    application.add_handler(CommandHandler("start", start))

    #application.add_handler(MessageHandler(filters.Regex("h7yf83f8gfrv098owpow0e08u$"), prod_descript))

    application.add_handler(MessageHandler(filters.Regex("^Просмотреть меню$"), get_category))

    application.add_handler(conv_handler)

    application.add_handler(conv_handler_prod)

    application.add_handler(InlineQueryHandler(inline_query_1))

    application.run_polling()









