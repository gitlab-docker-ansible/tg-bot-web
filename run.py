from init_app import init_flask
from main.bot_telegram import start_bot
import threading
import os

ff = init_flask()


class FlaskThread(threading.Thread):
    def run(self) -> None:
        ff.run(host=os.getenv('FLASK_RUN_HOST'))


class TelegramThread(threading.Thread):
    def run(self) -> None:
        start_bot()


if __name__ == "__main__":
    flask_thread = FlaskThread()
    flask_thread.start()

    start_bot()