import sys
#sys.path.append("..")
from database import Base, session
from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.types import String, Integer, Float


class User(Base):
	__abstract__ = True


class Admin(User):

	__tablename__ = 'admins'

	id = Column(Integer(), primary_key=True)
	name = Column(String())

	def __init__(self, name):
		self.name = name 


class Tg_User(User):

	__tablename__ = 'tg_users'

	id = Column(Integer(), primary_key=True)
	name = Column(String())

	def __init__(self, name):
		self.name = name



class Product(Base):

	__tablename__ = 'products'

	id = Column(Integer(), primary_key=True)
	category = Column(Integer(), ForeignKey('category.id'))
	name = Column(String())
	description = Column(String())
	price = Column(Float())
	image = Column(String())


	def __init__(self, name, category, description, price, image):
		self.name = name
		self.category = category
		self.description = description
		self.price = price
		self.image = image


class Category(Base):

	__tablename__ = 'category'

	id = Column(Integer(), primary_key=True)
	name = Column(String(), unique=True)
	product = relationship("Product")

	def __init__(self, name):
		self.name = name


	@staticmethod
	def get_all_category(self):
		return session.query(Category).all()







