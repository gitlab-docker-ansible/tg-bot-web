from crypt import methods
import os
#import sys
#sys.path.append("..")
from flask import Blueprint, render_template, request, redirect, url_for
from database import Base, session
from admin.models import Admin, Tg_User, Product, Category
from mytools import allowed_file
from werkzeug.utils import secure_filename
from database import BASEDIR
from myform import ProductForm, ProductFormEdit


admin = Blueprint('admin', __name__, template_folder='templates', static_folder='static')


@admin.route('/admin')
def index():


	return render_template('admin/dashboard/index.html')


@admin.route('/products')
def products():
	products_list = session.query(Product).all()
	category_list = session.query(Category).all()

	return render_template('admin/product/index.html', products_list=products_list, category_list=category_list)


@admin.route('/add_category', methods=['GET', 'POST'])
def add_category():
	if request.method == 'POST':
		name = request.form.get('name')
		session.add(Category(name))
		session.commit()
		return redirect(url_for('admin.products'))
	return render_template('admin/product/add_category.html')


@admin.route('/delete_product/<product_id>', methods=['GET'])
def delete_product(product_id):
	session.query(Product).filter(Product.id == int(product_id)).delete()
	return redirect(url_for('admin.products'))



@admin.route('/edit_product/<pr_id>', methods=['GET', 'POST'])
def edit_product(pr_id):
	
	product = session.query(Product).filter(Product.id == int(pr_id)).first()

	form = ProductFormEdit()

	form.category.choices = [(cat.id, cat.name) for cat in session.query(Category).all()]
	form.category.default = product.category

	if request.method == 'POST':
		name = form.name.data
		price = form.price.data
		description = form.description.data
		image = form.image.data
		imagename = ''
		if image and allowed_file(image.filename):
			imagename = secure_filename(image.filename)
			image.save(os.path.join(BASEDIR, imagename))
		category = form.category.data

		new_data = {
			Product.name: name, Product.category: int(category),
			Product.description: description, Product.price: price, 
			Product.image: imagename
		}
		for key, value in list(new_data.items()):
			if value == '':
				del new_data[key]
		print(new_data)


		session.query(Product).filter(Product.id == int(pr_id)).update(new_data)
		session.commit()

		return redirect(url_for('admin.products'))
	return render_template('admin/product/edit_product.html', form=form, product=product)



@admin.route('/add_product', methods=['GET', 'POST'])
def add_product():
	
	form = ProductForm()

	form.category.choices = [(cat.id, cat.name) for cat in session.query(Category).all()]

	if request.method == 'POST':

		name = form.name.data
		price = form.price.data
		description = form.description.data
		image = form.image.data
		if image and allowed_file(image.filename):
			imagename = secure_filename(image.filename)
			image.save(os.path.join(BASEDIR, imagename))
		else:
			imagename = 'default.png'
		category = form.category.data

		session.add(Product(name, int(category), description, price, imagename))
		session.commit()

		return redirect(url_for('admin.products'))
	
	return render_template('admin/product/add_product.html', form=form)


